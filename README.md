# osp-quakecon

#Quakecon at home RTCW server configuration.

## Docker

`docker build -t rtcw/osp-server-quakecon:latest .`

## Command

`/root/rtcw/wolfded.x86`

## Arguments

Remember to update `refpassword` and `rconpassword` in `main/server.cfg` or your command line arguments.

`+set dedicated 2 +set fs_game osp +set com_hunkmegs 64 +set com_zonemegs 32 +set vm_game 0 +set ttycon 0 +exec server.cfg +exec pbsv-eu.cfg +set g_password password +set refpassword password +set rconpassword password +map mp_ice`

`+set dedicated 2 +set fs_game osp +set com_hunkmegs 64 +set com_zonemegs 32 +set vm_game 0 +set ttycon 0 +exec server.cfg +exec pbsv-na.cfg +set g_password password +set refpassword password +set rconpassword password +map mp_ice`