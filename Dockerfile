FROM ubuntu:14.04

RUN dpkg --add-architecture i386 && \
    apt-get update && \
    apt-get install -y wget && \
    apt-get install -y libc6-i386 libc6:i386

RUN wget http://archive.debian.org/debian/pool/main/g/gcc-2.95/libstdc++2.10-glibc2.2_2.95.4-27_i386.deb  && \
          dpkg -i libstdc++2.10-glibc2.2_2.95.4-27_i386.deb && \
          rm -rf libstdc++2.10-glibc2.2_2.95.4-27_i386.deb
          
RUN mkdir /root/rtcw/
WORKDIR /root/rtcw/
RUN wget http://192.210.199.233/rtcw/files/osp-server.tar.gz
RUN tar -xvf osp-server.tar.gz
RUN rm -rf osp-server.tar.gz

EXPOSE 27960/udp

ENTRYPOINT ["/root/rtcw/wolfded.x86"]

CMD ["+set", "dedicated 2", "+set", "fs_game osp", "+set", "com_hunkmegs 64", "+set", "com_zonemegs 32", "+set", "vm_game 0", "+set", "ttycon 0", "+exec", "server.cfg", "+exec", "pbsv-eu.cfg", "+map", "mp_ice"]